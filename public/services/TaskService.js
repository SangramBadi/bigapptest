var app = angular.module('BigCompany');

app.service('TaskService', ['$http', 'globalUrl', function($http, globalUrl) {

    this.createTask = function(data) {
        return $http({
            url: globalUrl.baseurl + 'create',
            method: 'POST',
            data: data
        })
    }

    this.updateTask = function(data) {
        return $http({
            url: globalUrl.baseurl + 'update',
            method: 'PUT',
            data: data
        })
    }

     this.loadTask = function(data) {
        return $http({
            url: globalUrl.baseurl + 'all',
            method: 'GET'
        })
    }

    this.getById = function(id) {
        return $http({
            url: globalUrl.baseurl + 'id/' + id,
            method: 'GET'
        })
    }

    this.deleteById = function(id) {
        return $http({
            url: globalUrl.baseurl + 'delete/' + id,
            method: 'DELETE'
        })
    }

    this.search = function(data) {
        return $http({
            url: globalUrl.baseurl + 'search',
            method: 'POST',
            data: data
        })
    }

    
}]);