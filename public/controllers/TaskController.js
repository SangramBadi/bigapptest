var app = angular.module('BigCompany', []);

app.controller('TaskController', ['$scope', 'TaskService', function ($scope, TaskService) {

    function loadTask() {
        
        TaskService.loadTask().then(function (response) {
            $scope.loadTask = response.data;
            

            angular.forEach($scope.loadTask, function (value, key) {
                $scope.loadTask[key].endDate = moment(value.task_end_date).format('DD-MM-YYYY');
                $scope.loadTask[key].ctreatDate = moment(value.created_on).format('DD-MM-YYYY');
            });
           
        }, function (error) {
            swal('Error', 'Something went wrong.', 'error');
        });
    }

    function getById(id) {
        TaskService.getById(id).then(function (response) {
            var data = response.data[0];
            $scope.task.id = data.id;
            $scope.task.taskName = data.task_name;
            $scope.task.taskEndDate = data.task_end_date;
            $scope.endFormt = moment(data.task_end_date).format('DD-MM-YYYY');
            $scope.task.description = data.description;
            $scope.task.createdBy = data.created_by;
        }, function (error) {
            swal('Error', 'Something went wrong.', 'error');
        });
    }

    function init() {
        $scope.isEdit = false;
        $scope.task = {
            'id': '',
            'taskName': '',
            'taskEndDate': '',
            'description': '',
            'createdBy': ''
        }

        $scope.endDate = '';
        $scope.createDate = '';
        $scope.createdBy = '';
        $scope.taskName = '';

        $scope.currentPage = 1; 

        loadTask();
    }
    init();

    $scope.saveTask = function () {

        if ($scope.task.taskName == '') {
            $scope.errorName = true;
        } else {
            $scope.errorName = false;
        }

        if ($scope.task.taskEndDate == '') {
            $scope.errorEndDate = true;
        } else {
            $scope.errorEndDate = false;
            console.log($scope.task.taskEndDate);
            $scope.task.taskEndDate = moment($scope.task.taskEndDate).format('YYYY-MM-DD  HH:mm:ss');
        }

        if ($scope.task.createdBy == '') {
            $scope.errorCreatedBy = true;
        } else {
            $scope.errorCreatedBy = false;
        }

        console.log($scope.task);

        if ($scope.errorName == false && $scope.errorEndDate == false && $scope.errorCreatedBy == false) {

            TaskService.createTask($scope.task).then(function (response) {
                swal('Success', response.data.message, 'success');
                $scope.task = '';
                init();
            }, function (error) {
                swal('Error', 'Something went wrong.', 'error');
            });
        }
    }

    $scope.edit = function (id) {
        $scope.isEdit = true;
        getById(id);
    }

    $scope.updateTask = function () {
        
        if ($scope.task.taskName == '') {
            $scope.errorName = true;
        } else {
            $scope.errorName = false;
        }

        if ($scope.task.taskEndDate == '') {
            $scope.errorEndDate = true;
        } else {
            $scope.errorEndDate = false;
            console.log($scope.task.taskEndDate);
            $scope.task.taskEndDate = moment($scope.task.taskEndDate).format('YYYY-MM-DD  HH:mm:ss');
        }

        if ($scope.task.createdBy == '') {
            $scope.errorCreatedBy = true;
        } else {
            $scope.errorCreatedBy = false;
        }

        console.log($scope.task);

        if ($scope.errorName == false && $scope.errorEndDate == false && $scope.errorCreatedBy == false) {
            swal({
                title: "Are you sure?",
                text: "you want to update this data.",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    TaskService.updateTask($scope.task).then(function (response) {
                        init();
                        swal('Success', response.data.message, 'success');
                    }, function (error) {
                        swal('Error', 'Something went wrong.', 'error');
                    });
                }
            });
        }
    }

    $scope.removeTask = function (id) {

        var id = id;
        swal({
            title: "Are you sure?",
            text: "you want to delete this data.",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                TaskService.deleteById(id).then(function (response) {
                    loadTask();
                    swal('Success', response.data.message, 'success');
                }, function (error) {
                    swal('Error', 'Something went wrong.', 'error');
                });
            }
        });
    }

    $scope.search = function() {
        
        if($scope.endDate != '' && $scope.endDate != 'Invalid date') {
            $scope.endDate = moment($scope.endDate).format('YYYY-MM-DD  HH:mm:ss');
        } else {
            $scope.endDate = '';
        }

        if($scope.createDate != '' && $scope.createDate != 'Invalid date') {
            $scope.createDate = moment($scope.createDate).format('YYYY-MM-DD  HH:mm:ss');
        } else {
            $scope.createDate = '';
        }

        var searchData = {
            'endSearch' : $scope.endDate,
            'createSearch' : $scope.createDate,
            'createdBySearch' : $scope.createdBy,
            'taskName' : $scope.taskName
        }

        TaskService.search(searchData).then(function(response) {
            $scope.loadTask = response.data;

            angular.forEach($scope.loadTask, function (value, key) {
                $scope.loadTask[key].endDate = moment(value.task_end_date).format('DD-MM-YYYY');
                $scope.loadTask[key].ctreatDate = moment(value.created_on).format('DD-MM-YYYY');
            });

            $scope.currentPage = 1;

             $scope.viewby = 4;
            $scope.totalItems = $scope.loadTask.length;
          
            $scope.itemsPerPage = $scope.viewby;
            $scope.maxSize = 5;
        }, function(error) {

        });
    }

    $scope.reset = function() {
        init();
    }

}]);