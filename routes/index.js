var express = require('express');

var router = express.Router();

/* GET home page. */
router.get('/', function(req, resp) {
  resp.send('Welcome to big app company API.');
});

module.exports = router;
