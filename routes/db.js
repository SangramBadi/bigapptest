var mysql = require('mysql');

var connection = mysql.createConnection({
    host : '127.0.0.1',
    user: 'root',
    password: '',
    database: 'big_app_company_db',
    port: '3306'
});

connection.connect(
    function(err) {
        if(!err) {
            console.log("Database is connected");
        } else {
            console.log("Database is not connected" + err);
        }
    }
);

module.exports = connection;