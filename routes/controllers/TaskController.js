var express = require('express');
var db = require('./../db.js');

var router = express.Router();

/**
 * Task create API
 * taskId - it is generate 6 digit random number
 * created on - it is also generate in API
 */
router.post('/create', function (req, resp) {

    var insertSql = 'INSERT INTO task_table (task_name, task_end_date, description, created_on, task_id, created_by) VALUES(?,?,?,?,?,?)';

    var taskId = Math.floor(100000 + Math.random() * 900000);

    db.query(insertSql, [req.body.taskName,
    req.body.taskEndDate,
    req.body.description,
    (new Date()).toISOString().substring(0, 19).replace('T', ' '),
        taskId,
    req.body.createdBy], function (error, result, fields) {

        if (error) {
            resp.status('500');
            resp.send(error);
        }

        if (result) {
            resp.status('200');
            resp.json({
                message: 'Inserted Successfully.',
                'insertedId': result.insertId
            });
        }

        return resp;
    });
});

/**
 * Task update API, it update record using unique task id
 */
router.put('/update', function (req, resp) {

    var updateSql = 'UPDATE task_table SET task_name = ?, description = ?, created_by = ? WHERE id = ?';
    console.log(updateSql);
    db.query(updateSql, [req.body.taskName,
    req.body.description,
    req.body.createdBy,
    req.body.id], function (error, result, fields) {

        if (error) {
            resp.status('500');
            resp.send(error);
        }

        if (result) {
            resp.status('200');
            resp.json({
                message: 'Updated Successfully.'
            });
        }
    });
    return resp;
});

/*
get all task list
*/
router.get('/all', function (req, resp) {
    db.query('SELECT * FROM task_table', function (error, result, fields) {

        if (error) {
            resp.status('500');
            resp.json({
                message: 'Something went wrong.'
            });

        }

        if (result) {
            resp.status('200');
            resp.send(result);
        }

        return resp;
    });
});

/*
get task by id
*/
router.get('/id/:id', function (req, resp) {

    if (req.params.id) {
        db.query('SELECT * FROM task_table WHERE id = ' + req.params.id, function (error, result, fields) {

            if (error) {
                resp.status('500');
                resp.json({
                    message: 'Something went wrong.'
                });

            }

            if (result) {
                resp.status('200');
                resp.send(result);
            }
        });
    } else {
        resp.status('400');
    }

    return resp;
});

/*
    Delete from task table by id
*/
router.delete('/delete/:id', function (req, resp) {

    if (req.params.id) {

        var deleteSql = "DELETE FROM task_table WHERE id = " + req.params.id;

        db.query(deleteSql, function (error, result, fields) {
            if (error) {
                resp.status('500');
                resp.json({
                    message: 'Something went wrong.'
                });
            }

            if (result) {
                resp.status('200');
                resp.json({
                    message: 'Record is deleted.'
                });
            }
        })
    } else {
        resp.status('400');
    }

    return resp;
});

/*
saerch by task end date
*/
router.post('/search', function (req, resp) {

    if(req.body.endSearch != '' && req.body.createSearch != '') {
        var searchQuery = 'SELECT * FROM task_table WHERE task_end_date >= "' + req.body.endSearch + '" AND created_on >= "' + req.body.createSearch + '" AND created_by LIKE "%' + req.body.createdBySearch + '%" AND task_name LIKE "%' + req.body.taskName + '%" OR description LIKE "%' +  req.body.taskName  + '%"';
    } else if(req.body.endSearch != '' && req.body.createSearch == '') {
        var searchQuery = 'SELECT * FROM task_table WHERE task_end_date >= "' + req.body.endSearch + '" AND created_by LIKE "%' + req.body.createdBySearch + '%" AND task_name LIKE "%' + req.body.taskName + '%" OR description LIKE "%' +  req.body.taskName  + '%"';
    } else if(req.body.endSearch == '' && req.body.createSearch != '') {
        var searchQuery = 'SELECT * FROM task_table WHERE created_on >= "' + req.body.createSearch + '" AND created_by LIKE "%' + req.body.createdBySearch + '%" AND task_name LIKE "%' + req.body.taskName + '%" OR description LIKE "%' +  req.body.taskName  + '%"';
    } else {
        var searchQuery = 'SELECT * FROM task_table WHERE created_by LIKE "%' + req.body.createdBySearch + '%" AND task_name LIKE "%' + req.body.taskName + '%" OR description LIKE "%' +  req.body.taskName  + '%"';
    }

    console.log(searchQuery);

    db.query(searchQuery, function (error, result, fields) {

        if (error) {
            resp.status('500');
            resp.json({
                message: 'Something went wrong.'
            });

        }

        if (result) {
            resp.status('200');
            resp.send(result);
        }
    });

    return resp;
});

module.exports = router;